package com.kostrych.banksystem.service;

import com.kostrych.banksystem.domain.PrimaryAccount;
import com.kostrych.banksystem.domain.SavingsAccount;

import java.security.Principal;

public interface AccountService {
    PrimaryAccount createPrimaryAccount();
    SavingsAccount createSavingsAccount();

    public void deposit(String accountType, double amount, Principal principal);
}
